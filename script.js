// Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript?

//це мьоуктура, яка дозволяє об'єктам використовувати властивості та методи інших об'єктів

// Для чого потрібно викликати super() у конструкторі класу-нащадка?

//У JavaScript, конструктор класу-нащадка може викликати super() у своєму конструкторі для виклику конструктора його батьківського класу. Це дозволяє успадкуванню правильно налаштовувати об'єкти та ініціалізувати їхні властивості.

//---------------------------------------------------

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        console.log(`Користувача звати: ${this._name}`);
        return this._name;
    }
    set name(newName) {
        this._name = newName;
    }

    get age() {
        console.log(`Вік користувача "${this._name}": ${this._age}`);
        return this._age;
    }
    set age(newAge) {
        this._age = newAge;
    }

    get salary() {
        console.log(`Зарплата користувача "${this._name}": ${this._salary}`);
        return this._salary;
    }
    set salary(newSalary) {
        this._salary = newSalary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    get lang() {
        console.log(`Мови програмування користувача "${this.name}": ${this._lang.join(", ")}`);
        return this._lang;
    }
    set lang(newLang) {
        this._lang = newLang;
    }

    get salary() {
        let upSalary;
        if (Array.isArray(this._lang)) {
            upSalary = ((this._lang.length * 0.5) + 1) * this._salary;
        }
        console.log(`Зарплата користувача "${this.name}": ${upSalary}`);
        return upSalary;
    }
}

// Приклад використання
const danilo = new Programmer("Danylo Korchevhyi", 21, 7000, ["Js", "Pythone"]);
console.log("------------------------");
console.log(danilo.name);
console.log(danilo.age);
console.log(danilo.lang);
console.log(danilo.salary);
const oleg = new Programmer("Oleg Pirochinkov", 32, 9500, ["Js", "Pythone", "C++"]);
console.log("------------------------");
console.log(oleg.name);
console.log(oleg.age);
console.log(oleg.lang);
console.log(oleg.salary);

const dima = new Programmer("Dmytro Brazhnyk", 22, 9000, ["JS", "C#", "sign language", "meme"]);
console.log("------------------------");
console.log(dima.name);
console.log(dima.age);
console.log(dima.lang);
console.log(dima.salary);




